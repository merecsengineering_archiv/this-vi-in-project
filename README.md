# This VI in Project

This VI in Project - Menu Plugin created in LabVIEW 2016 - Developed by Elmar Schneider	

1. Follow the instructions here: https://forums.ni.com/t5/LabVIEW-Shortcut-Menu-Plug-Ins/How-to-install-plug-ins-that-you-download/ta-p/3517848
2. Restart LabVIEW
3. Right-Click on a SubVI AND working with a LabVIEW project: "This VI in Project" appears in the popup menu.


